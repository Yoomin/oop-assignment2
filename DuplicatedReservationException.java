package assignment;

public class DuplicatedReservationException extends Exception{
    //이미 있는 자리 예약시도
    public DuplicatedReservationException(){
        super(" is already reserved.");
    }
}
