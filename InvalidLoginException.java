package assignment;

public class InvalidLoginException extends Exception{
    //존재하지 않는 ID 혹은 틀린 비밀번호로 로그인 시도
    public InvalidLoginException(){
        super("Login failed.");

    }
}
