package assignment;

public class InvalidMenuException extends Exception{
    //User의 Manager의 메뉴 접근
    public InvalidMenuException() {
        super("Invalid Access");
    }
    //없는 메뉴 번호
    public InvalidMenuException(String m){
        super(m);
        System.out.println(" is an invalid menu number.");

    }
}
