package assignment;

public class Manager extends Member{
    public Manager(int mu, String id, String password) {
        super(1, id, password);
    }

    public void startManagerProgram(){
        manPrint();
        int n = manMenu();
        manProgram(n);
    }


    public void manPrint(){
        System.out.println("*******영화관 관리*******");
        System.out.println("1. 영화관 정보");
        System.out.println("2. 유저 정보");
        System.out.println("3. 종료");
    }

    public int manMenu() {
        System.out.print("메뉴를 선택해주세요 : ");
        int n = sc.nextInt();
        return n;
    }


    public void manProgram(int u) {
        switch(u) {
            case 1:
                theaInfo();
                startManagerProgram();
            case 2:
                userInfo();
                startManagerProgram();
            case 3:
                break;

        }
    }

    //1. 영화관 정보
    private void theaInfo() {
        System.out.println("예매율 현황");
        System.out.println("--------------------------");
        //예매율 높은 세 영화 예매 좌석 수 출력
        System.out.println("--------------------------");
        System.out.print("Press enter to go back to Theater Management");
        String n = sc.next();
    }


    //2. 유저 정보
    private void userInfo() {
        System.out.print("찾으려는 ID: ");
        String id = sc.next();

        Member mb = FindById(id);
        System.out.println(mb.getId() + " 고객님이 발행한 티켓의 수: " + this.getPersonalTicketList().size());
        System.out.println("--------------------------");
        //티켓 번호, 영화 정보, 좌석 출력
        System.out.println("--------------------------");
    }


}
