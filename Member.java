package assignment;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Member {
    Scanner sc = new Scanner(System.in);
//    Ticket ticket = new Ticket(new Member(0, "",""), new Movie("",""), "");
//	Ticket ti = new Ticket("", "");
//	private int ticketNumber;

    private String id;
    private String password;
    private int mu;


//    private Ticket ticket;
    private ArrayList<Ticket> personalTicketList;
    //	private Movie movie;

    ArrayList<Member> memberList = new ArrayList<>();

    //객체 생성 함수
    public Member(int mu, String id, String password) {
        this.mu = mu;
        this.id = id;
        this.password = password;

        //예매한 영화 객체 생성
        //영화 좌석번호 티켓번호

        this.personalTicketList = new ArrayList<>();
    }

    public void startMemberProgram(){
        memberPrint();
        int n = memberMenu();
        memberProgram(n);
    }


    public void memberPrint(){
        System.out.println("*******영화 예매 프로그램*******");
        System.out.println("1. 로그인");
        System.out.println("2. 회원가입");
        System.out.println("3. 종료");
    }

    public int memberMenu() {
        System.out.print("메뉴를 선택해주세요 : ");
        int n = sc.nextInt();
        return n;
    }


    public void memberProgram(int u) {
        switch(u) {
            case 1:
                login();
                break;
            case 2:
                MemberJoin();
                break;
            case 3:
                System.out.println("영화 예매 프로그램을 종료합니다.");
                break;

        }
    }

    //1. 로그인
    private void login() {
        System.out.println("*******로그인*******");
        System.out.print("Id : ");
        String uid = sc.next();
        System.out.print("Password : ");
        String upw = sc.next();

        Member mb = FindById(uid);
        try {
            if(mb == null) {
                //등록되지 않은 id
                throw new InvalidLoginException();
            }else if(mb.getPassword().equals(upw)){
                //로그인 잘 되었는지 확인차 출력
                System.out.println("[" + mb.getId() + "]님께서 로그인 하셨습니다.");
                Ticket ticket = new Ticket(null);
                ticket.startTicketProgram(this);
            }else{
                //비밀번호 틀렸을때
                throw new InvalidLoginException();
            }
        }catch (InvalidLoginException e){
            System.out.println(e.getMessage());
            startMemberProgram();
        }


    }


    //2. 회원가입
    private void MemberJoin(){
        System.out.println("*******회원가입*******");
        System.out.print("Id : ");
        String nid = sc.next();
        System.out.print("Password : ");
        String npw = sc.next();
        System.out.print("Manager : ");
        int mu = sc.nextInt();

        try{
            if(FindById(nid) != null) {
                throw new DuplicatedIdException();
            }else {
                memberList.add(new Member(mu, nid, npw));
                //나중에 빼도 됨--------ㄱ
                System.out.println(nid + "님 가입을 축하드립니다.");
                startMemberProgram();
            }
        }catch (DuplicatedIdException e){
            System.out.print(nid);
            System.out.println(e.getMessage());
            startMemberProgram();
        }

    }


//    private boolean idCheck(String id) {
//        boolean check = true;
//        Member member = FindById(id);
//        if(member == null)
//            check = false;
//        return check;
//    }


    public Member FindById(String uid) {
        for(Member m : memberList) {
            if(m.getId().equals(uid)) {
                return m;
            }
        }
        return null;
    }

    ////=============티멧번호 생성 ============
//		private int newTicketNum(){
//			return ticketNumber = Counter.getNextNumber();
//		}
//		private static class Counter{
//			//count증가
//			private static int getNextNumber() {
//				count++;
//				return count;
//			}
//		}




    //
//	    private void ticketNumJoin(int e) {
//	    	this.setTicketNumber(e);
////	    	users.add(new User(nid, npw));
//	    }




//	    public int getTicketNumber() {
//			return ticketNumber;
//		}


//    public void setTicketNumber(int ticketNumber) {
//        this.personalTicketList.add(ticketNumber);
//    }


//    public void setTicketList(ArrayList<Ticket> ticketList) {
//        this.personalTicketList = ticketList;
//    }


    //필요없음
    @Override
    public String toString() {
        return "ID : " + id + " PW : " + password;
    }

    public String getId() {
        return id;
    }

//    public void setId(String id) {
//        this.id = id;
//    }

    public String getPassword() {
        return password;
    }

//    public void setPassword(String password) {
//        this.password = password;
//    }

    public int getMu() {
        return mu;
    }

    public void setMu(int mu) {
        this.mu = mu;
    }

    public ArrayList<Ticket> getPersonalTicketList() {
        return personalTicketList;
    }

//    public void setPersonalTicketList(ArrayList<Ticket> personalTicketList) {
//        this.personalTicketList = personalTicketList;
//    }

    public ArrayList<Member> getMemberList() {
        return memberList;
    }

    public void setMemberList(ArrayList<Member> memberList) {
        this.memberList = memberList;
    }

}


