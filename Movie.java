package assignment;

import java.util.ArrayList;
import java.util.Scanner;



public class Movie {

    Scanner sc = new Scanner(System.in);

//	User us = new User("", "");
//	Movie mo = new Movie(null, null, 0);

    private String seat_row;
    private String seat_col;
    private int ticketNumber;
//    private static int count;
//
    private Ticket ticket;
//    private Member member;

//	public Ticket() {};

    private String title;
    private String showtime;
    private char[][] seat;

    ArrayList<Movie> movlist = new ArrayList<Movie>();

    //처음에 영화 등록시 필요

//	 public ArrayList<Movie> addMov(Movie m1) {
//
//		 for(Movie m : movlist) {
//			 movlist.add(m);
//		 }
//		 return movlist;
//	 }

    public Movie(String title, String showtime_s, String showtime_f) {
        this.title = title;
        this.showtime = showtime_s + ":00 - " + showtime_f + ":00";

        this.seat= new char[7][7];
        Init_seat(this);
        //행렬에 없는 번호로 초기화
//        this.seat_row = "7";
//        this.seat_col = "7";
    }

    public void appendTicket(Ticket t){
        ArrayList<Ticket> allTicketlist = ticket.getAllTicketlist();
        allTicketlist.add(t);
        ticket.setAllTicketlist(allTicketlist);
    }

    public void startMovieProgram(Member m){
        moviePrint();
        int n = movieMenu();
        movieProgram(n,m);
    }

    public void moviePrint() {
        System.out.println("*******영화 목록*******");
        movlist = getMovlist();
        for(Movie m : movlist) {
            int i = 1;
            System.out.println(m.toString());
            i ++;
        }
    }

    public void moviePrintWithNum() {
        System.out.println("*******영화 목록*******");
        for(Movie m : movlist) {
            int i = 1;
            System.out.println(i + "." + m.toString());
            i ++;
        }
    }

    public int movieMenu() {
        System.out.println("");
        System.out.println("1. 예매");
        System.out.println("2. 종료");
        System.out.print("메뉴를 선택해주세요 : ");
        int n = sc.nextInt();
        return n;
    }


    public void movieProgram(int u, Member m) {

//밑에 다시 입력해주세요 때문에 while true 썼는데 주
        while(true) {
            switch (u) {
                case 1://예매
                    moviePrintWithNum();
                    System.out.println("");
                    System.out.print("예매할 영화를 선택해주세요 : ");
                    int n = sc.nextInt();
                    if(m.getMu()==1){
                        Movie tm = movlist.get(n);
                        //매니저일 경우 추가 출력
                        manInfo(tm);
                    }
                    movieReser(n, m);
                    break;

                case 2://종료 (Ticket 프로그램으로)
                    break;
            }
        }
    }
    public void manInfo(Movie tm) {
        System.out.println("\"" + tm.title +"\" 영화의 좌석 예매 점유율:");
        System.out.println("\"" + tm.title +"\" 영화의 총 매출액:");
    }

//======================================
//
	 //좌석 초기화. 처음 등록시 필요

	 public void Init_seat(Movie m) {
		 char col = 'A';
		 char row = '1';
		 for (int i = 0; i < 7; i++) {
			 for (int j = 0; j < 7; j++) {
				 m.seat[i][j] = 'O';
			 }
		 }
		 for (int i = 1; i < 7; i++) {
			 m.seat[i][0] = col++;
			 }
		 for (int j = 1; j < 7; j++) {
			 m.seat[0][j] = row++;
			 }
		 }

	 public void Print_seat() {
		 System.out.println("*****좌 석*****");
		 for (char[] index : seat) {
			 for (char temp : index) {
				 System.out.printf("%c ", temp);
				 }
			 System.out.println("");
			 }
		 System.out.println("**************");
		 }



	 //좌석 예약
	 private void movieReser(int t, Member m) {
         //선택한 t번 영화
         Movie mov = new Movie("", "","");
         //영화 io 에따라 t에 -1 할지 말지 수정
         mov = movlist.get(t);
         m.toString();
         Print_seat();
         System.out.print("좌석을 선택해주세요(ex A1) : ");
         String st = sc.next();


         //좌석
         char[] ip = st.toCharArray();
         int col = 0, row = 0;
         col = (int) ip[0] - 64;
         row = (int) ip[1] - 48;
         while(true) {
             try {
                 if (col > 6) {
                     throw new NotExistSeatException();
                 } else if (row > 6) {
                     throw new NotExistSeatException();
                 } else if (seat[col][row] == 'X') {
                     System.out.printf("%c%c좌석을 예매했습니다. %n%n", ip[1], ip[2]);
                     // %%지우기????       <-------------------------

                     //, movlist.get(t), st 밑에서 이거 뺌
                     ticket.appendTicket(new Ticket(m));
                     break;
                 } else if (seat[col][row] != 'X') {
                     throw new DuplicatedReservationException();
                 }
             } catch (NotExistSeatException e) {
                 System.out.print(st);
                 e.getMessage();
             } catch (DuplicatedReservationException e) {
                 System.out.print(st);
                 e.getMessage();
             }
         }
     }

//    public String getSeat_row() {
//        return seat_row;
//    }
//
//    public void setSeat_row(String seat_row) {
//        this.seat_row = seat_row;
//    }
//
//    public String getSeat_col() {
//        return seat_col;
//    }
//
//    public void setSeat_col(String seat_col) {
//        this.seat_col = seat_col;
//    }
//
//
//    public int getTicketNumber() {
//        return ticketNumber;
//    }
//
//
//    public void setTicketNumber(int ticketNumber) {
//        this.ticketNumber = ticketNumber;
//    }

    public ArrayList<Movie> getMovlist() {
        return movlist;
    }

    public void setMovlist(ArrayList<Movie> movlist) {
        this.movlist = movlist;
    }

    @Override
    public String toString() {
        return "제목 : " + title + " / 상영시간 : " + showtime;
    }
}
