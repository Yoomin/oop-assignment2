package assignment;

public class NotExistSeatException extends Exception{
    //존재하지 않는 자리 예약시도
    public NotExistSeatException(){
        super(" does not exist.");
    }
}
