package assignment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Theater {
    public static void main(String[] args) {

        Scanner inputStreamMovie = null;

        Scanner inputStreamUser = null;
        PrintWriter outputStreamUser = null;

        Member m = new Member(0, "","");
        Movie movie = new Movie("a","a","a");

        //영화 텍스트파일 읽기
        try{
            inputStreamMovie = new Scanner(new FileInputStream("/Users/ym/Desktop/Yoom_InteliJ/src/assignment/MovieList.txt"));
            //a는 텍스트 파일의 첫줄로 영화의 개수 의미
            String a = inputStreamMovie.nextLine();
            String li;
            ArrayList<Movie> movieList = new ArrayList<>();

            for(int i = 0; inputStreamMovie.hasNextLine(); i++){
                li = inputStreamMovie.nextLine();
                String[] originalMov = li.split("/");

                String title = originalMov[0];
                String s = originalMov[1];
                String f = originalMov[2];

                movieList.add(new Movie(title, s, f));
            }
            movie.setMovlist(movieList);
//            System.out.println(movieList.toString());
            inputStreamMovie.close();

        } catch (FileNotFoundException e) {
            System.out.println("Problem opening movie files.");
            System.exit(0);
        }

        try{
            inputStreamUser = new Scanner(new FileInputStream("/Users/ym/Desktop/Yoom_InteliJ/src/assignment/UserList.txt"));

            //유저텍스트 파일 첫줄의 사이즈만큼 배열 크기 정하기..?
            //a는 텍스트 파일의 첫줄로 유저의 수 의미
            String a = inputStreamUser.nextLine();

            String line = null;
            ArrayList<Member> memberList = new ArrayList<>();

            for(int i = 0; inputStreamUser.hasNextLine(); i++){
                line = inputStreamUser.nextLine();

                String[] originalUser = line.split(" ");
                String strmu = originalUser[0];
                int mu = Integer.parseInt(strmu);
                String id = originalUser[1];
                String password = originalUser[2];

                memberList.add(new Member(mu, id, password));
            }
            m.setMemberList(memberList);
            outputStreamUser = new PrintWriter(new FileOutputStream("UserListFinal.txt"));
            inputStreamUser.close();
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
            System.out.println("Problem opening user files.");
            System.exit(0);
        }


        //영화 프로그램 시작
//        Member m = new Member(0, "","");

        m.startMemberProgram();


        //마지막 유저 리스트 출력
        ArrayList<Member> finalMemberList = m.getMemberList();
        int count = finalMemberList.size();
        outputStreamUser.println(count);
        for(int i = 0; i < count; i++){
            outputStreamUser.println(finalMemberList.get(i).getMu() + " " + finalMemberList.get(i).getId() + " " + finalMemberList.get(i).getPassword());
        }
        outputStreamUser.close();



    }
}
